

//选中全选按钮，下面的radio全部选中 
var selAll = document.getElementById("selAll"); 
function selectAll(){ 
  var obj = document.getElementsByName("checkAll");
  if(document.getElementById("selAll").checked == false){ 
    for(var i=0; i<obj.length; i++){ 
      obj[i].checked=false; 
    } 
  }else { 
    // 遍历选中
    for(var i=0; i<obj.length; i++){   
      obj[i].checked=true;  
    } 
  }    
  choseCtrl(); 
}


//当选中所有的时候，全选按钮会勾上 
function setSelectAll(){ 
  var obj=document.getElementsByName("checkAll"); 
  var count = obj.length; 
  var selectCount = 0; 
  for(var i = 0; i < count; i++){ 
    if(obj[i].checked == true){ 
      selectCount++;  
    } 
  } 
  if(count == selectCount){ 
    document.all.selAll.checked = true; 
  } 
  else { 
    document.all.selAll.checked = false; 
  } 
}


// radio 按钮绑定
$("body").delegate(".radio-btn","change",function(){
    choseCtrl();    
});
  




// select改变时激发全选判断，radio判断操作
$("body").delegate(".select-wrap select", "change",function(){
    $("#selAll").prop("checked",false);
    choseCtrl();
})


// 通过radio选择 判断当前可执行何种操作？
function choseCtrl() {
  var arr = [],a,b,c;
  $("input[name='checkAll']:checkbox").each(function(){ 
    if($(this).prop("checked")){
      arr.push($(this).attr("value")); 
    }
    // 广告的状态码，暂时自定，0代表刚新建未投放，4代表暂停,3代表正常,5代表删除
     a = arr.indexOf("0");
     b = arr.indexOf("4");
     c = arr.indexOf("3");

     if(a == -1 && b !== -1 && c == -1){ //暂停状态
        
        $(".buttons-wrap .btn1, .btn2").removeClass("state-disabled").removeAttr("disabled");
        $(".btnb").show();
        $(".btna").hide();
     }else if(a !==-1 && c == -1) { // 新建未投放｜｜新建与暂停共选
        
        $(".buttons-wrap .btn0").addClass("state-disabled").attr("disabled");
        $(".buttons-wrap .btn1, .btn2").removeClass("state-disabled").removeAttr("disabled");
        $(".btna").show();
        $(".btnb").hide();
     } else if (a ==-1 && b == -1 && c == -1){ // 未勾选
      
        $(".buttons-wrap button").addClass("state-disabled").attr("disabled");
     }else if(a == -1 && b == -1 && c !== -1){ //正常状态
       
        $(".btn0, .btn2").removeClass("state-disabled").removeAttr("disabled");
        $(".buttons-wrap .btn1").addClass("state-disabled").attr("disabled");
     } else { // 三种状态都有
        $(".buttons-wrap .btn0, .btn1").addClass("state-disabled").attr("disabled");
        $(".buttons-wrap .btn2").removeClass("state-disabled").removeAttr("disabled");
     }            
  })
}


// angular判断是否radio选中并保存值供给URL参数
angular.module("myApp").controller('AddStyleCtrl', function($scope, User,$location,$modal,$log){ 
  
  // 获取广告资源列表
  User.getAdResource(function (data) {
    if (data.reason != null) {
       window.location.href = "../../login.html";
    } else {
      $scope.data = data.adResource;
      
    }
  }); 
  // 获取广告类型
  User.getAdType(function(result){
    if (result.reason != null) {
      window.location.href = "../../login.html";
      
    } else {
      $scope.result = result.adCategory;
    }
  });


  // 判断radio是否选中。选中则拿到ID存入数组
  $scope.selected = []; 
  $scope.selectedTags = []; 
  
  var updateSelected = function(action,id,value){ 
    if(action == 'add' && $scope.selected.indexOf(id) == -1){ 
      $scope.selected.push(id); 
      $scope.selectedTags.push(value); 
    } 
    if(action == 'remove' && $scope.selected.indexOf(id)!=-1){ 
      var idx = $scope.selected.indexOf(id); 
      $scope.selected.splice(idx,1); 
      $scope.selectedTags.splice(idx,1); 
    } 
  } 
  
  $scope.updateSelection = function($event, id){ 
    var checkbox = $event.target; 
    var action = (checkbox.checked?'add':'remove'); 
    updateSelected(action,id,checkbox.value); 
  } 
  
  $scope.isSelected = function(id){ 
      $scope.aid = id;
    return $scope.selected.indexOf(id)>=0; 
  } 

  // 暂停 删除操作
  $scope.updatead = function($event){
    $scope.uid = $scope.aid;
    $scope.state = $event.target.value;
    User.updatestate({id: $scope.uid, status: $scope.state},function(data){
      if (data.result === "success") {
         alert("广告资源状态更新成功!");
         $(".buttons-wrap button").addClass("state-disabled").attr("disabled");
         $("#selAll").prop("checked",false);
         User.getAdResource(function (data) {
              $scope.data = data.adResource;

          }); 

      } else{
        alert(data.reason);
      }
    })

  }



  // 打开详细页弹窗
   $scope.openwin = function(id, update_ts){  //打开模态 
        $scope.items = id;
        $scope.itemts = update_ts;
        // console.log($scope.getId);
        var modalInstance = $modal.open({
            templateUrl : 'myModelContent.html',  //指向上面创建的视图
            controller : 'ModalInstanceCtrl',// 初始化模态范围
            size : 'lg', //大小配置
            resolve : {
                items : function(){
                    return $scope.items;
                },
                itemts : function(){
                    return $scope.itemts;
                }
            }
        })
        modalInstance.result.then(function(selectedItem){  
            $scope.selected = selectedItem;
        },function(){
            // $log.info('Modal dismissed at: ' + new Date())
        })
    }



});

// modal 控制器 
angular.module('myApp').controller('ModalInstanceCtrl',function($scope,User,$modalInstance,$location,items,itemts){ //依赖于modalInstance
       $scope.getId = items;
       $scope.updatets = itemts;
       User.getAdResource(function (data) {
            $scope.data = data.adResource;    
      }); 
        $scope.newset = function(){  
            $modalInstance.dismiss(); //关闭
            $location.url('/Advertising_first?id='+items); //跳转投放界面
        };
        $scope.cancel = function(){
            $modalInstance.dismiss('cancel'); // 退出
        }
    })










 

    

    