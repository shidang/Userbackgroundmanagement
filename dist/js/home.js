
var app = angular.module("myApp", ['ngResource','ui.router','ui.bootstrap','angular.morris-chart']);



app.config(['$stateProvider','$urlRouterProvider',function ($stateProvider, $urlRouterProvider) {
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/home");
        // Now set up the states
        $stateProvider
            .state("home", { //主页
                url: "/home",
                templateUrl: '/pages/examples/home_con.html'
            })
            .state("AdManagement", { //广告管理
                url: "/AdManagement",
                templateUrl: '/pages/examples/AdManagement.html'
            })
            .state("data_repot", { //数据报表
                url: "/data_repot",
                templateUrl: '/pages/examples/data_repot.html'
            })
            .state("Advertising_first", { //投放广告1
                url: "/Advertising_first?id",
                templateUrl: '/pages/examples/Advertising_first.html'
            })
            .state("Advertising_second", { //投放广告2
                url: "/Advertising_second?id",
                templateUrl: '/pages/examples/Advertising_second.html'
            })
            .state("Newadvertising", { //新建广告
                url: "/Newadvertising",
                templateUrl: '/pages/examples/Newadvertising.html'
            })                 
    }]);


 // app.constant('prefix', 'https://alpha.aiyingwujia.com');
 app.constant('prefix', 'http://backing.test.aiyingwujia.com:3000');

// 页面API请求接口
app.factory('User', ['$resource', 'prefix', function ($resource,prefix) {
    return $resource(''+prefix+'/api/admin/ad/resource', {}, {
      getAdResource: { // 获取广告资源列表
        method: 'GET',
        isArray: false,
        withCredentials: true
      },
      getAdType:{ //获取广告分类
        url: prefix+'/api/admin/ad/category',
        method:'GET',
        isArray:false,
        withCredentials: true
      },
      postAdResource:{//添加广告资源
        url: prefix+'/api/admin/ad/resource',
        method:'POST',
        isArray:false,
        withCredentials: true
      },
      targetadvertising:{ //设置广告投放目标
        url: prefix+'/api/admin/ad/resource/first',
        method:'POST',
        isArray:false,
        withCredentials: true
      },
      timeadvertising:{ //设置广告投放时间
        url: prefix+'/api/admin/ad/resource/second',
        method:'POST',
        isArray:false,
        withCredentials: true
      },
      updatestate:{ // 更新广告状态 
        url: prefix+'/api/admin/ad/resource/update',
        method:'POST',
        isArray:false,
        withCredentials: true
      },
      adlogout:{ //退出登录
        url: prefix+'/api/admin/ad/user/logout',
        method:'GET',
        isArray:false,
        withCredentials: true
      }      

    });

  }]);






// 首页投放广告数据
app.controller("customersCtrl", function($scope, User) {
    User.getAdResource(function (data) { // 获取投放广告
        if(data.reason != null ) {
           window.location.href="../../login.html";
         
        } else {
          $scope.data = data.adResource; 
          
        }
        
    });
    $scope.logout = function(){
        User.adlogout(function(re){ //退出登录
            if(re.result == "success"){
                window.location.href="login.html";
            }
        }) 
    }
    
});





