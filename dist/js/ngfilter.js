

// 投放人群由数字转换为可读可识别的字符串
angular.module("myApp").filter('family', function () {
  var genderHash = {
  	4: "育儿",
    3:'已婚',
    2: '新婚',
    1: '单身',
    0: '全部'
  };
  return function (input) {
    return genderHash[input] || '未选';
  };
});


// 广告状态由数字转换为可读可识别的字符串
angular.module("myApp").filter('status', function () {
  var genderHash = {
  	0: '未投放',
  	1: '未设置时间',
    2: '投放确认',
    3: '投放中',
    4: '暂停',
    5: '删除',
    
  };
  return function (input) {
    return genderHash[input] || 'none';
  };
});


// 投放活跃度由数字转换为可读可识别的字符串
angular.module("myApp").filter('vital', function () {
  var genderHash = {
    2: '不太活跃',
    1: '一般',
    0: '活跃'
  };
  return function (input) {
    return genderHash[input] || '未选';
  };
});



// 投放活跃度由数字转换为可读可识别的字符串
angular.module("myApp").filter('acti', function () {
  var genderHash = {
    1: '偏祝福',
    0: '偏许愿'
  };
  return function (input) {
    return genderHash[input] || '未选';
  };
});


// 省份编号转换为可读可识别的字符串
angular.module("myApp").filter('distri', function () {
  var genderHash = {
  	"" : "全部",
  	"010": "北京",
  	"021": "上海",
  	"023": "重庆",
  	"022": "天津",
  	"020": "广州",
  	"025": "南京",
  	"027": "武汉",
  	"028": "成都",
  	"029": "西安",
  	"0571": "杭州",
  	"0591": "福州",
  	"0592": "厦门",
  	"0595": "泉州",
    "0755" : '深圳',
    "0311": "石家庄",
    "0791": "武昌",
    "0731": "长沙"
  };
  return function (input) {
    return genderHash[input] || '未选';
  };
});


// 时间格式转换
angular.module("myApp").filter('time', function () {
  return function (input) {
    input= String(input||[]);
    var len = input.length;
    if(len === 4){
    	input = input.replace(/(.{2})/,'$1:')
    } else {
    	input = input.replace(/(.{1})/,'$1:')
    }
    return input;
    };
});
 



