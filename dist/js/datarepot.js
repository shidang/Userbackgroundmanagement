
  angular.module("myApp").controller("dataCtrl", function($scope, $http) {
        $http.get("data.json")
        .success(function (response) {
          $scope.data = response.records;
          $scope.selectedName = $scope.data[0].Name;
          $scope.selectedPlace = $scope.data[0].palce;
        });
    });
  // line chart
  angular.module("myApp").controller('MyCtrl', function($scope){
    $scope.xaxis = 'y';
    $scope.yaxis = '["a"]'; 
    $scope.data = [
      { y: "2006-07", a: 100},
      { y: "2006-06", a: 75},
      { y: "2006-05", a: 50},
      { y: "2006-04", a: 75},
      { y: "2006-03", a: 50},
      { y: "2006-02", a: 75},
      { y: "2006-01", a: 100}
    ];   
  });


// 时间选择
angular.module("myApp").controller('dateCtrl', function ($scope) {
     var dateRange1 = new pickerDateRange('date1', {
        isTodayValid : true,
        startDate : '2016-05-14',
        endDate : '2016-05-18',
        needCompare : false,
        defaultText : ' 至 ',
        autoSubmit : true,
        inputTrigger : 'input_trigger1',
        theme : 'ta',
        success : function(obj) {
          $("#dCon2").html('开始时间 : ' + obj.startDate + '<br/>结束时间 : ' + obj.endDate);
        }
      });
});





