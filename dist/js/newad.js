

var keyurl;
 
//  广告上传
$("body").delegate("#adType","change",function(){
    var token, keys, type; 
    type = $(this).val();
    if(type === "chose"){
      $("#adImgVideo span").hide();
      $("#imgPlace").hide();
    }
    if(type ==="1"){
      $("#adImgVideo span").show().text("上传图片格式限定为jpg格式，分辨率为1242*2208,大小不超过2M")
      $("#imgPlace").show();
      $.get('http://backing.test.aiyingwujia.com:3000/upload/imagetoken',function(re){         
          token = re.uptoken;
          keys = re.save_key;
          $("#imgPlace").html('<img src="../../dist/img/upload.jpg" class="photo"><input type="file" id="avatar">')
          upload(token,'http://cdn.image.aiyingwujia.com',keys,{title : "Image files", extensions : "jpg"})
      });
    }
    // https://alpha.aiyingwujia.com/upload/imagetoken
    // if(type === "video"){
    //   $("#adImgVideo span").show().text("上传视频格式限定为mp4格式，分辨率为1024 * 576，大小不超过10M")
    //   $("#imgPlace").show();
    //   $.get('https://alpha.aiyingwujia.com/upload/videotoken',function(re){         
    //       token = re.uptoken;
    //       keys = re.save_key;
    //       $("#imgPlace").html('<img src="../../dist/img/upload.jpg" class="photo"><input type="file" id="avatar">')
    //       upload(token,'http://cdn.video.aiyingwujia.com',keys, {title : "mp4 files", extensions : "mp4"})
    //   });
    // }

})

//  七牛 upload sdk
function upload(token,domains,keys,limit){
  var uploader = Qiniu.uploader({
      runtimes: 'html5,html4',    //上传模式,依次退化
      browse_button: 'avatar',       //上传选择的点选按钮，**必需**
      uptoken: token,
      unique_names: false, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
      save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
      domain: domains,   //bucket 域名，下载资源时用到，**必需**
      get_new_uptoken: false,  //设置上传文件的时候是否每次都重新获取新的token
      container: 'imgPlace',           //上传区域DOM ID，默认是browser_button的父元素，
      max_file_size: '10mb',           //最大文件体积限制
      max_retries: 3,                   //上传失败最大重试次数
      dragdrop: true,                   //开启可拖曳上传
      drop_element: 'imgPlace',        //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
      chunk_size: '5mb',                //分块上传时，每片的体积
      auto_start: true,                 //选择文件后自动上传，若关闭需要自己绑定事件触发上传  
      init: {
          'FilesAdded': function(up, files) {

              plupload.each(files, function(file) {
                  // 文件添加进队列后,处理相关的事情
                                
              });
          },
          'BeforeUpload': function(up, file) {
                 // 每个文件上传前,处理相关的事情
                 
                 //计算文件大小
                 console.log(file);
                var size = Math.floor(file.size/1024); 
                if(size > 10000) {
                    alert("上传文件不得超过10M!");
                    return false;
                }
          },
          'FileUploaded': function(up, file, info) {
                 var domain = up.getOption('domain');
                 var res = JSON.parse(info);
                 keyurl = res.key;
                 var sourceLink = domain + '/' + keyurl; //获取上传成功后的文件的Url
                 if(file.type.indexOf('image') !== -1){
                  $("#imgPlace img").attr("src", sourceLink);
                } else {
                  $("#imgPlace img").remove();
                 $("#imgPlace").append("<video class=\"photo\" style='max-width:100%;position:relative;z-index:2;' controls='controls' src=\""+sourceLink+"\"/>");
                }
          },
          'Error': function(up, err, errTip) {
                 //上传出错时,处理相关的事情
                 console.log(err);
                 console.log(errTip);
          },
          'Key': function(up, file) {
              // 若想在前端对每个文件的key进行个性化处理，可以配置该函数
              // 该配置必须要在 unique_names: false , save_key: false 时才生效

              var key = keys;
              return key
          }
      },
      filters : {
            max_file_size : '10mb',
            prevent_duplicates: true,
            //Specify what files to browse for
             mime_types: [ limit
             //限定mp4后缀上传格式上传
             //限定jpg,gif,png后缀上传
             ]
        },
    });
}











  
// 获取广告分类
angular.module("myApp").controller("newadCtrl", function($rootScope, $scope, User,$location) {
  User.getAdType(function(data){
    if (data.reason != null) {
      window.location.href = "../../login.html";
      
    } else {
      $scope.data = data.adCategory;
    }
    
  });
  $scope.postresource = function () {
      var adname = $(".adname").val();
      var adtype = $("#adType").val();
      //提交数据
      var params = {cid: adtype, name: adname,src:keyurl};
      User.postAdResource(params, function (data) {
        if (data.result === "success") {
            User.getAdResource({skip:0,limit:1}, function (re) {                
              $scope.idNum = re.adResource[0].id;
              $location.url('/Advertising_first?id='+$scope.idNum);

          });     
        }else{
          alert(data.reason +',请填写完整广告信息，包括广告名,广告类型,附件!');
        }

      });
  };
    
});


