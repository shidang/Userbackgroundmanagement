// 投放目标传递url参数
angular.module("myApp").controller('adingCtrl1', function ($scope,$location) {
      $scope.changeUrl = function(){
          $scope.urlid =  $location.search();
          $scope.sex = $("input[name='sex']:checked").val();
          $scope.age = $("input[name='age']:checked").val();
          if($scope.age == "diy"){
            $scope.minage = $('.sel1 option:selected').val();
            $scope.maxage = $('.sel2 option:selected').val();
          }else {
            $scope.minage = '';
            $scope.maxage = '';
          }
          $scope.place = $("input[name='place']:checked").val();
          if($scope.place == "diy"){
            $scope.place = $('.pla option:selected').val();
          } else {
            $scope.place = '';
          }
          $scope.family = $("input[name='marriage']:checked").val();
          if($scope.family == "diy"){
            $scope.family = $("input[name='marriage1']:checked").val();
          } else {
            $scope.family = '';
          }
          $scope.level = $("input[name='grade']:checked").val();
          if($scope.level =="5"){
            $scope.minlevel = "1";
            $scope.maxlevel = "5"; 
          }else if($scope.level == "20"){
            $scope.minlevel = "6";
            $scope.maxlevel = "20"; 
          } else {
            $scope.minlevel = "21";
            $scope.maxlevel = '';
          }
          $scope.liveness = $("input[name='liveness']:checked").val();
          $scope.activi = $("input[name='wish']:checked").val();
          $location.url('/Advertising_second?id='+$scope.urlid.id+'&sex='+$scope.sex+'&age_min='+$scope.minage+'&age_max='+$scope.maxage+'&district='+$scope.place+'&family='+$scope.family+'&level_min='+$scope.minlevel+'&level_max='+$scope.maxlevel+'&vitality='+$scope.liveness+'&activity='+$scope.activi);
      };





});


// 投放时间与提交数据
angular.module("myApp").controller('adingCtrl2', function ($scope,$location,User) {
    // 时间选择
   var dateRange1 = new pickerDateRange('date_demo1', {
      isTodayValid : true,
      //isSingleDay : true,
      startDate : '2016-05-12',
      endDate : '2016-07-10',
      needCompare : false,
      defaultText : ' 至 ',
      target : 'datePicker_demo1',
      calendars : 2,
      stopToday: false,
      theme : 'ta',
      shortOpr : false,
      success : function(obj) {
        $("#dCon_demo1").html('开始时间 : ' + obj.startDate + '<br/>结束时间 : ' + obj.endDate);
      }
    });



     $scope.adpost = function () {      
        //提交数据
        var params = $location.search();

        $scope.beginDate = $("#startDate").val().replace(/-/g, "");
        $scope.endDate = $("#endDate").val().replace(/-/g, "");
        $scope.beginTime = $(".startime").val().replace(/:/g, "");
        $scope.endTime = $(".endtime").val().replace(/:/g, "");
       // 判断是否设定投放目标
        if(params.sex){
          User.targetadvertising(params, function (data) {
            if (data.result === "success") {
                User.timeadvertising({id:params.id,beginDate:$scope.beginDate,endDate:$scope.endDate,beginTime:$scope.beginTime,endTime:$scope.endTime},
                  function(data){
                   if (data.result === "success") {
                      $location.url('/AdManagement');
                    } else{
                      alert(data.reason +',请填写完整广告信息件!');
                    }
                })
            }else{
              alert(data.reason +',请返回上一步!');
            }
          });
        } else {
          User.timeadvertising({id:params.id,beginDate:$scope.beginDate,endDate:$scope.endDate,beginTime:$scope.beginTime,endTime:$scope.endTime},
              function(data){
               if (data.result === "success") {
                   $location.url('/AdManagement');
                } else{
                  alert(data.reason +',请填写完整广告信息件!');
                }
          })
        }
         
      };
    $scope.prevstep = function(){
      $scope.urlid =  $location.search();
      $location.url('/Advertising_first?id='+$scope.urlid.id);
    }

});




// 自定义显示隐藏
function show(his) {
  if($(his).val()=="diy"){
    $(his).parent().next().show();
  }else {
    $(his).parent().siblings(".controlnone").hide();
  }
}